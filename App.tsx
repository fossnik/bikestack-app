import React from 'react';
import {Ionicons} from '@expo/vector-icons';

import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';

// Auth stack
import AuthLoadingScreen from './src/components/screens/authLoadingScreen';
import WelcomeScreen from './src/components/screens/welcomeScreen';
import SignUpScreen from './src/components/screens/account/signUpScreen';
import ConfirmationScreen from './src/components/screens/account/confirmationScreen';
import SignInScreen from './src/components/screens/account/signInScreen';
import ForgetPasswordScreen from './src/components/screens/account/forgetPasswordScreen';

// App stack
import NewEntryScreen from './src/components/screens/newEntryScreen';
import TripLogScreen from './src/components/screens/tripLogScreen';
import SettingsScreen from './src/components/screens/settingScreen';
import ChartScreen from './src/components/screens/chartScreen';

// Amplify
import Amplify from 'aws-amplify';
import awsconfig from './src/aws-exports';
import {Colors} from './src/components/CommonStyles';

Amplify.configure(awsconfig);

// Configurations and options for the AppTabNavigator
const routeConfigs = {
    NewEntry: {
        screen: NewEntryScreen,
        navigationOptions: {
            tabBarLabel: 'New Entry',
            tabBarIcon: ({tintColor}) => (
                <Ionicons style={{fontSize: 26, color: tintColor}} name='ios-create'/>
            )
        }
    },
    TripLog: {
        screen: TripLogScreen,
        navigationOptions: {
            tabBarLabel: 'Trip Log',
            tabBarIcon: ({tintColor}) => (
                <Ionicons style={{fontSize: 26, color: tintColor}} name='md-bicycle'/>
            )
        }
    },
    Charts: {
        screen: ChartScreen,
        navigationOptions: {
            tabBarLabel: 'Charts',
            tabBarIcon: ({tintColor}) => (
                <Ionicons style={{fontSize: 26, color: tintColor}} name='ios-podium'/>
            )
        }
    },
    Settings: {
        screen: SettingsScreen,
        navigationOptions: {
            tabBarLabel: 'Settings',
            tabBarIcon: ({tintColor}) => (
                <Ionicons style={{fontSize: 26, color: tintColor}} name='ios-settings'/>
            )
        }
    },
};

const tabNavigatorConfig = {
    initialRouteName: 'TripLog',
    tabBarPosition: 'top',
    swipeEnabled: false,
    animationEnabled: true,
    navigationOptions: {
        tabBarVisible: true,
    },
    tabBarOptions: {
        showLabel: true,
        activeTintColor: Colors.white,
        inactiveTintColor: Colors.grey,
        style: {
            backgroundColor: Colors.gunmetal,
            marginTop: 24,
        },
        labelStyle: {
            fontSize: 12,
            fontWeight: 'bold',
            margin: 4,
        },
        indicatorStyle: {
            height: 4,
        },
        showIcon: true,
    },
};

// @ts-ignore
const AppTabNavigator = createMaterialTopTabNavigator(routeConfigs, tabNavigatorConfig);

const AuthStackNavigator = createStackNavigator({
    Welcome: {
        screen: WelcomeScreen,
        navigationOptions: () => ({
            title: 'Welcome to Bike Stack',
            headerBackTitle: 'Back',
        }),
    },
    SignUp: {
        screen: SignUpScreen,
        navigationOptions: () => ({
            title: 'Create a new account',
        }),
    },
    Confirm: {
        screen: ConfirmationScreen,
        navigationOptions: () => ({
            title: 'Enter confirmation code',
        }),
    },
    SignIn: {
        screen: SignInScreen,
        navigationOptions: () => ({
            title: 'Log in to your account',
        }),
    },
    ForgetPassword: {
        screen: ForgetPasswordScreen,
        navigationOptions: () => ({
            title: 'Create a new password',
        }),
    },
});

const AppNavigator = createSwitchNavigator({
        AuthLoading: AuthLoadingScreen,
        Auth: AuthStackNavigator,
        App: AppTabNavigator,
    },
    {
        initialRouteName: 'AuthLoading',
    });

export default createAppContainer(AppNavigator);
