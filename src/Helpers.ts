import {Alert} from 'react-native';
import _ from 'lodash';
import {Excursion} from './components/types';

export function validateEntryData(entryData: Excursion): boolean {
    if (_.isNaN(entryData.distance)) return false;
    if (_.isNaN(entryData.duration)) return false;
    if (!_.isDate(entryData.date)) return false;

    return true;
}


export function handleError(
    desc: string,
    err: {
        code: string,
        message: string,
        name: string,
    },
): void {
    if (__DEV__) {
        console.error(desc, err);
    } else {
        Alert.alert(desc + ':\n' + err.message);
    }
}

export function handleSuccess(desc: string, alert?: boolean): void {
    if (alert) Alert.alert(desc);
    if (__DEV__) console.log(desc);
}

export function getSum(values: number[]): number {
    return values.reduce((a, b) => a + b);
}

export function titleCase(input: string): string {
    return input.replace(/\b[a-z]/g, c => c.toUpperCase());
}

export const fourMetrics = ['distance', 'duration', 'notes', 'date'];
