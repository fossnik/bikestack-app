/* tslint:disable */
//  This file was automatically generated and should not be edited.

export type CreateExcursionInput = {
  id?: string | null,
  userSub: string,
  distance: number,
  duration: number,
  date: string,
  notes?: string | null,
};

export type UpdateExcursionInput = {
  id: string,
  userSub?: string | null,
  distance?: number | null,
  duration?: number | null,
  date?: string | null,
  notes?: string | null,
};

export type DeleteExcursionInput = {
  id?: string | null,
};

export type ModelExcursionFilterInput = {
  id?: ModelIDFilterInput | null,
  userSub?: ModelIDFilterInput | null,
  distance?: ModelFloatFilterInput | null,
  duration?: ModelIntFilterInput | null,
  date?: ModelStringFilterInput | null,
  notes?: ModelStringFilterInput | null,
  and?: Array< ModelExcursionFilterInput | null > | null,
  or?: Array< ModelExcursionFilterInput | null > | null,
  not?: ModelExcursionFilterInput | null,
};

export type ModelIDFilterInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
};

export type ModelFloatFilterInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  contains?: number | null,
  notContains?: number | null,
  between?: Array< number | null > | null,
};

export type ModelIntFilterInput = {
  ne?: number | null,
  eq?: number | null,
  le?: number | null,
  lt?: number | null,
  ge?: number | null,
  gt?: number | null,
  contains?: number | null,
  notContains?: number | null,
  between?: Array< number | null > | null,
};

export type ModelStringFilterInput = {
  ne?: string | null,
  eq?: string | null,
  le?: string | null,
  lt?: string | null,
  ge?: string | null,
  gt?: string | null,
  contains?: string | null,
  notContains?: string | null,
  between?: Array< string | null > | null,
  beginsWith?: string | null,
};

export type CreateExcursionMutationVariables = {
  input: CreateExcursionInput,
};

export type CreateExcursionMutation = {
  createExcursion:  {
    __typename: "Excursion",
    id: string,
    userSub: string,
    distance: number,
    duration: number,
    date: string,
    notes: string | null,
  } | null,
};

export type UpdateExcursionMutationVariables = {
  input: UpdateExcursionInput,
};

export type UpdateExcursionMutation = {
  updateExcursion:  {
    __typename: "Excursion",
    id: string,
    userSub: string,
    distance: number,
    duration: number,
    date: string,
    notes: string | null,
  } | null,
};

export type DeleteExcursionMutationVariables = {
  input: DeleteExcursionInput,
};

export type DeleteExcursionMutation = {
  deleteExcursion:  {
    __typename: "Excursion",
    id: string,
    userSub: string,
    distance: number,
    duration: number,
    date: string,
    notes: string | null,
  } | null,
};

export type GetExcursionQueryVariables = {
  id: string,
};

export type GetExcursionQuery = {
  getExcursion:  {
    __typename: "Excursion",
    id: string,
    userSub: string,
    distance: number,
    duration: number,
    date: string,
    notes: string | null,
  } | null,
};

export type ListExcursionsQueryVariables = {
  filter?: ModelExcursionFilterInput | null,
  limit?: number | null,
  nextToken?: string | null,
};

export type ListExcursionsQuery = {
  listExcursions:  {
    __typename: "ModelExcursionConnection",
    items:  Array< {
      __typename: "Excursion",
      id: string,
      userSub: string,
      distance: number,
      duration: number,
      date: string,
      notes: string | null,
    } | null > | null,
    nextToken: string | null,
  } | null,
};

export type OnCreateExcursionSubscription = {
  onCreateExcursion:  {
    __typename: "Excursion",
    id: string,
    userSub: string,
    distance: number,
    duration: number,
    date: string,
    notes: string | null,
  } | null,
};

export type OnUpdateExcursionSubscription = {
  onUpdateExcursion:  {
    __typename: "Excursion",
    id: string,
    userSub: string,
    distance: number,
    duration: number,
    date: string,
    notes: string | null,
  } | null,
};

export type OnDeleteExcursionSubscription = {
  onDeleteExcursion:  {
    __typename: "Excursion",
    id: string,
    userSub: string,
    distance: number,
    duration: number,
    date: string,
    notes: string | null,
  } | null,
};
