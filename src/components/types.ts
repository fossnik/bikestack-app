export interface Excursion {
    date: Date,
    distance: number,
    duration: number,
    id?: string,
    notes?: string,
    dateShort?: string,
    speed?: number,
}

export enum ChartMode {
    monthOverMonth,
    allData,
}
