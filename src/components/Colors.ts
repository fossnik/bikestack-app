'use strict';

export default {
    white: '#fff',
    navy: '#18121e',
    daisy: '#e9e7da',
    rust: '#984B43',
    red: '#d10000',
    green: '#008500',
    stem: '#636b46',
    gunmetal: '#233237',
    apricot: '#f7882f',
    skyBlue: '#328CC1',
    yellow: '#eac67a',
    placeholder: '#adb4bc',
    blue: '#5059ae',
    pink: '#b44666',
    grey: '#fff9',
    black: '#000000',
};
