import React from 'react';
import {
    StyleSheet, TextStyle, ViewStyle, ImageStyle,
    TouchableOpacity, Text, View,
    TouchableWithoutFeedback,
    SafeAreaView, KeyboardAvoidingView,
    StatusBar, Keyboard, Image, ActivityIndicator,
} from 'react-native';
import {Container, Item, Input, Icon} from 'native-base';
import CommonStyles, {Colors} from '../../CommonStyles';
import * as helper from '../../../Helpers';
import Auth from '@aws-amplify/auth';

const logo = require('../../images/logo.png');

export default class SignInScreen extends React.Component<{
    navigation: any;
}, {}> {
    state = {
        username: '',
        password: '',
        isHidden: false,
        loading: false,
    };

    onChangeText(key, value) {
        this.setState({[key]: value});
    }

    async signIn() {
        this.setState({loading: true});
        const {username, password} = this.state;
        await Auth.signIn(username, password)
            .then(() => {
                helper.handleSuccess('Log in successful');
                this.props.navigation.navigate('AuthLoading');
            })
            .catch(err => helper.handleError('An error occurred when signing in', err));
    };

    render() {
        const {isHidden, loading} = this.state;
        const message = this.props.navigation.getParam('message');

        return <SafeAreaView style={styles.container}>
            <StatusBar/>
            <KeyboardAvoidingView enabled style={styles.container} behavior='padding'>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View style={styles.container}>
                        {!isHidden && <Image source={logo} style={styles.logo}/>}
                        {message && <Text style={styles.message}>{message}</Text>}
                        <Container style={styles.infoContainer}>
                            <Item rounded style={styles.itemStyle}>
                                <Icon active name='person' style={styles.iconStyle}/>
                                <Input
                                    style={styles.input}
                                    placeholder='Username'
                                    placeholderTextColor={Colors.placeholder}
                                    keyboardType='email-address'
                                    returnKeyType='next'
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    // @ts-ignore
                                    onSubmitEditing={(event) => this.refs.SecondInput._root.focus()}
                                    onChangeText={value => this.onChangeText('username', value)}
                                    onFocus={() => this.setState({isHidden: true})}
                                />
                            </Item>
                            <Item rounded style={styles.itemStyle}>
                                <Icon active name='lock' style={styles.iconStyle}/>
                                <Input
                                    style={styles.input}
                                    placeholder='Password'
                                    placeholderTextColor={Colors.placeholder}
                                    returnKeyType='go'
                                    autoCapitalize='none'
                                    autoCorrect={false}
                                    secureTextEntry={true}
                                    ref='SecondInput'
                                    onChangeText={value => this.onChangeText('password', value)}
                                    onFocus={() => this.setState({isHidden: true})}
                                />
                            </Item>
                            <TouchableOpacity
                                onPress={() => this.signIn()}
                                style={styles.buttonStyle}>
                                {
                                    loading ? <ActivityIndicator size='large' color={Colors.white}/> :
                                    <Text style={styles.buttonText}>
                                        Sign In
                                    </Text>
                                }
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => this.props.navigation.navigate('ForgetPassword')}
                            >
                                <Text style={styles.pageLink}>Forget password ?</Text>
                            </TouchableOpacity>
                        </Container>
                    </View>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        </SafeAreaView>
    }
}

const styles = StyleSheet.create<{
    container: ViewStyle;
    infoContainer: ViewStyle;
    logo: ImageStyle;
    input: TextStyle;
    itemStyle: ViewStyle;
    iconStyle: ViewStyle;
    buttonStyle: ViewStyle;
    buttonText: TextStyle;
    pageLink: TextStyle;
    message: TextStyle;
}>({
    container: {
        ...CommonStyles.container,
    },
    infoContainer: {
        ...CommonStyles.infoContainer,
        flexDirection: 'column',
    },
    logo: {
        width: 200,
        height: 200,
        margin: 20,
        borderRadius: 40,
        alignSelf: 'center',
    },
    input: {
        ...CommonStyles.input,
    },
    itemStyle: {
        ...CommonStyles.itemStyle,
    },
    iconStyle: {
        ...CommonStyles.iconStyle,
    },
    buttonStyle: {
        ...CommonStyles.blackButton,
        width: 200,
    },
    buttonText: {
        ...CommonStyles.buttonText,
    },
    pageLink: {
        textAlign: 'center',
        paddingHorizontal: 20,
        fontSize: 20,
        color: Colors.white,
    },
    message: {
        fontSize: 16,
        color: Colors.yellow,
        paddingVertical: 8,
    },
});
