import React from 'react';
import {
    StyleSheet, TextStyle, ViewStyle, ImageStyle,
    Text, View,
    KeyboardAvoidingView, SafeAreaView,
    TouchableOpacity, TouchableWithoutFeedback,
    StatusBar, Keyboard, Animated,
} from 'react-native';
import {Container, Item, Input, Icon} from 'native-base';
import CommonStyles, {Colors} from '../../CommonStyles';
import * as helper from '../../../Helpers';
import Auth from '@aws-amplify/auth';

const logo = require('../../images/logo.png');

export default class ForgetPasswordScreen extends React.Component<{
    navigation: any;
}, {}> {
    state = {
        username: '',
        authCode: '',
        newPassword: '',
        password2: '',
        fadeIn: new Animated.Value(0),
        fadeOut: new Animated.Value(1),
        isHidden: false,
    };

    onChangeText(key, value) {
        this.setState({[key]: value});
    }

    // Request a new password
    async forgotPassword() {
        const {username, newPassword, password2} = this.state;
        if (newPassword.length < 7 || newPassword !== password2) return;

        await Auth.forgotPassword(username)
            .then(() => {
                helper.handleSuccess('New code sent', true);
                this.props.navigation.navigate('Confirm', {
                    username: username,
                    operationType: 'forgotPassword',
                    newPassword: this.state.newPassword,
                });
            })
            .catch(err => helper.handleError('An error occurred when setting new password', err));
    }

    fadeIn() {
        Animated.timing(this.state.fadeIn, {
            toValue: 1,
            duration: 1000,
            useNativeDriver: true,
        }).start();
        this.setState({isHidden: true});
    }

    fadeOut() {
        Animated.timing(this.state.fadeOut, {
            toValue: 0,
            duration: 700,
            useNativeDriver: true,
        }).start();
        this.setState({isHidden: false});
    }

    componentDidMount() {
        this.fadeIn();
    }

    render() {
        const {fadeOut, fadeIn, isHidden} = this.state;

        return <SafeAreaView style={styles.container}>
            <StatusBar/>
            <KeyboardAvoidingView enabled style={styles.container} behavior='padding'>
                <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss}>
                    <View style={styles.container}>
                        <Animated.Image source={logo} style={[styles.logo,
                            isHidden ? {opacity: fadeIn} : {
                                opacity: fadeOut,
                                position: 'absolute',
                        }]}/>
                        <Container style={styles.infoContainer}>
                            <View style={{flex: 1}}>
                                {/* Username */}
                                <Item rounded style={styles.itemStyle}>
                                    <Icon active name='person' style={styles.iconStyle}/>
                                    <Input
                                        style={styles.input}
                                        placeholder='Username'
                                        placeholderTextColor={Colors.placeholder}
                                        keyboardType='email-address'
                                        returnKeyType='go'
                                        autoCapitalize='none'
                                        autoCorrect={false}
                                        onChangeText={value => this.onChangeText('username', value)}
                                        onFocus={this.fadeOut.bind(this)}
                                        onEndEditing={this.fadeIn.bind(this)}
                                    />
                                </Item>
                                {/* New password section */}
                                <Item rounded style={styles.itemStyle}>
                                    <Icon active name='lock' style={styles.iconStyle}/>
                                    <Input
                                        style={styles.input}
                                        placeholder='New password'
                                        placeholderTextColor={Colors.placeholder}
                                        returnKeyType='next'
                                        autoCapitalize='none'
                                        autoCorrect={false}
                                        secureTextEntry={true}
                                        // @ts-ignore
                                        onChangeText={value => this.onChangeText('newPassword', value)}
                                        onFocus={this.fadeOut.bind(this)}
                                        // @ts-ignore
                                        onSubmitEditing={(event) => this.refs.ThirdInput._root.focus()}
                                    />
                                </Item>
                                {/* password2 */}
                                <Item rounded style={[styles.itemStyle,
                                    this.state.newPassword !== this.state.password2 && {
                                        backgroundColor: Colors.rust,
                                    }]}>
                                    <Icon active name='lock' style={styles.iconStyle}/>
                                    <Input
                                        style={styles.input}
                                        placeholder='Password Confirm'
                                        placeholderTextColor={Colors.placeholder}
                                        returnKeyType='next'
                                        autoCapitalize='none'
                                        autoCorrect={false}
                                        secureTextEntry={true}
                                        ref='ThirdInput'
                                        // @ts-ignore
                                        onSubmitEditing={(event) => this.refs.FourthInput._root.focus()}
                                        onChangeText={value => this.onChangeText('password2', value)}
                                        onEndEditing={this.fadeIn.bind(this)}
                                    />
                                </Item>
                                <TouchableOpacity
                                    onPress={() => this.forgotPassword()}
                                    style={styles.buttonStyle}>
                                    <Text style={styles.buttonText}>
                                        Send Code
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </Container>
                    </View>
                </TouchableWithoutFeedback>
            </KeyboardAvoidingView>
        </SafeAreaView>
    }
}

const styles = StyleSheet.create<{
    container: ViewStyle;
    infoContainer: ViewStyle;
    logo: ImageStyle;
    input: TextStyle;
    itemStyle: ViewStyle;
    iconStyle: ViewStyle;
    buttonStyle: ViewStyle;
    buttonText: TextStyle;
}>({
    container: {
        ...CommonStyles.container,
    },
    infoContainer: {
        ...CommonStyles.infoContainer,
    },
    logo: {
        ...CommonStyles.logo,
    },
    input: {
        ...CommonStyles.input,
    },
    itemStyle: {
        ...CommonStyles.itemStyle,
    },
    iconStyle: {
        ...CommonStyles.iconStyle,
    },
    buttonStyle: {
        ...CommonStyles.blackButton,
    },
    buttonText: {
        ...CommonStyles.buttonText,
    },
});
