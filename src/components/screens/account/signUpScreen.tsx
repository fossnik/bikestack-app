import React from 'react';
import {
    StyleSheet, TextStyle, ViewStyle,
    Text, View, TouchableOpacity,
    TouchableWithoutFeedback, KeyboardAvoidingView,
    SafeAreaView, StatusBar, Keyboard,
    Modal, FlatList, ActivityIndicator,
} from 'react-native';
import CommonStyles, {Colors} from '../../CommonStyles';
import * as helper from '../../../Helpers';
import {Container, Item, Input, Icon} from 'native-base';
import Auth from '@aws-amplify/auth';

export default class SignUpScreen extends React.Component<{
    navigation: any;
}, {}> {
    state = {
        email: '',
        password: '',
        password2: '',
        loading: false,
    };

    onChangeText(key, value) {
        this.setState({[key]: value});
    }

    async signUp() {
        const {password, password2, email} = this.state;
        const username = email;

        if (password.length < 7 || password !== password2) return;

        this.setState({loading: true});

        await Auth.signUp({
            username,
            password,
            attributes: {email},
        })
            .then(() => {
                helper.handleSuccess('Sign up successful - redirect to confirmation screen');
                this.props.navigation.navigate('Confirm', {
                    username: username,
                    operationType: 'initialSignup',
                });
            })
            .catch(err => helper.handleError('An error occurred when signing up', err));
    }

    render() {
        const {loading} = this.state;

        return (
            <SafeAreaView style={styles.container}>
                <StatusBar/>
                <KeyboardAvoidingView enabled style={styles.container} behavior='padding'>
                    <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss}>
                        <View style={styles.container}>
                            <Container style={styles.infoContainer}>
                                <View style={styles.itemContainer}>
                                    {/* email section */}
                                    <Item rounded style={styles.itemStyle}>
                                        <Icon active name='mail' style={styles.iconStyle}/>
                                        <Input
                                            style={styles.input}
                                            placeholder='Email'
                                            placeholderTextColor={Colors.placeholder}
                                            keyboardType='email-address'
                                            returnKeyType='next'
                                            autoCapitalize='none'
                                            autoCorrect={false}
                                            secureTextEntry={false}
                                            ref='FirstInput'
                                            // @ts-ignore
                                            onSubmitEditing={(event) => this.refs.SecondInput._root.focus()}
                                            onChangeText={value => this.onChangeText('email', value)}
                                        />
                                    </Item>
                                    {/* password section */}
                                    <Item rounded style={styles.itemStyle}>
                                        <Icon active name='lock' style={styles.iconStyle}/>
                                        <Input
                                            style={styles.input}
                                            placeholder='Password'
                                            placeholderTextColor={Colors.placeholder}
                                            returnKeyType='next'
                                            autoCapitalize='none'
                                            autoCorrect={false}
                                            secureTextEntry={true}
                                            ref='SecondInput'
                                            // @ts-ignore
                                            onSubmitEditing={(event) => this.refs.ThirdInput._root.focus()}
                                            onChangeText={value => this.onChangeText('password', value)}
                                        />
                                    </Item>
                                    {/* password2 */}
                                    <Item rounded style={[styles.itemStyle,
                                        this.state.password !== this.state.password2 && {
                                            backgroundColor: Colors.rust,
                                        }]}>
                                        <Icon active name='lock' style={styles.iconStyle}/>
                                        <Input
                                            style={styles.input}
                                            placeholder='Password Confirm'
                                            placeholderTextColor={Colors.placeholder}
                                            returnKeyType='next'
                                            autoCapitalize='none'
                                            autoCorrect={false}
                                            secureTextEntry={true}
                                            ref='ThirdInput'
                                            // @ts-ignore
                                            onSubmitEditing={(event) => this.refs.FourthInput._root.focus()}
                                            onChangeText={value => this.onChangeText('password2', value)}
                                        />
                                    </Item>
                                    <TouchableOpacity
                                        onPress={() => this.signUp()}
                                        style={styles.buttonStyle}>
                                        {
                                            loading ? <ActivityIndicator size='large' color={Colors.white}/> :
                                                <Text style={styles.buttonText}>
                                                    Sign Up
                                                </Text>
                                        }
                                    </TouchableOpacity>
                                </View>
                            </Container>
                        </View>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create<{
    container: ViewStyle;
    infoContainer: ViewStyle;
    itemContainer: ViewStyle;
    input: TextStyle;
    itemStyle: ViewStyle;
    iconStyle: ViewStyle;
    buttonStyle: ViewStyle;
    buttonText: TextStyle;
}>({
    container: {
        ...CommonStyles.container,
        backgroundColor: Colors.stem,
    },
    infoContainer: {
        ...CommonStyles.infoContainer,
        flexDirection: 'column',
    },
    itemContainer: {
        borderRadius: 12,
        padding: 10,
    },
    input: {
        ...CommonStyles.input,
    },
    itemStyle: {
        ...CommonStyles.itemStyle,
        width: 300,
    },
    iconStyle: {
        ...CommonStyles.iconStyle,
    },
    buttonStyle: {
        ...CommonStyles.blackButton,
    },
    buttonText: {
        ...CommonStyles.buttonText,
    },
});
