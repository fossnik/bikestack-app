import React from 'react';
import {
    StyleSheet, TextStyle, ViewStyle,
    Text, View, TouchableOpacity,
    KeyboardAvoidingView,
    SafeAreaView, StatusBar,
} from 'react-native';
import CommonStyles, {Colors} from '../../CommonStyles';
import * as helper from '../../../Helpers';
import {Item, Input, Icon} from 'native-base';
import Auth from '@aws-amplify/auth';

export default class ConfirmationScreen extends React.Component<{
    navigation: any;
}, {}> {
    state = {
        authCode: '',
    };

    onChangeText(key, value) {
        this.setState({[key]: value});
    }

    async confirm() {
        const username = this.props.navigation.getParam('username');
        const {authCode} = this.state;

        const operationType = this.props.navigation.getParam('operationType');

        if (operationType === 'initialSignup') {
            await Auth.confirmSignUp(username, authCode)
                .then(() => {
                    helper.handleSuccess('Confirm sign up successful');
                    this.props.navigation.navigate('SignIn', {
                        message: 'You may log into your new account',
                    });
                })
                .catch(err => helper.handleError('An error occurred when submitting confirmation', err));
        } else if (operationType === 'forgotPassword') {
            const newPassword = this.props.navigation.getParam('newPassword');

            await Auth.forgotPasswordSubmit(username, authCode, newPassword)
                .then(() => {
                    helper.handleSuccess('New password submitted successfully');
                    this.props.navigation.navigate('SignIn', {
                        message: 'You may sign in with your new password',
                    });
                })
                .catch(err => helper.handleError('An error occurred when submitting new password', err));
        }
    }

    async resendSignUp() {
        const username = this.props.navigation.getParam('username');
        await Auth.resendSignUp(username)
            .then(() => helper.handleSuccess('Confirmation code resent successfully'))
            .catch(err => helper.handleError('An error occurred when requesting confirmation code', err));
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar/>
                <KeyboardAvoidingView enabled style={styles.container} behavior='padding'>
                    <View style={[styles.itemContainer, {
                        backgroundColor: Colors.gunmetal,
                    }]}>
                        <Item rounded style={styles.itemStyle}>
                            <Icon active name='md-apps' style={styles.iconStyle}/>
                            <Input
                                style={styles.input}
                                placeholder='Confirmation code'
                                placeholderTextColor={Colors.placeholder}
                                keyboardType='numeric'
                                returnKeyType='done'
                                autoCapitalize='none'
                                autoCorrect={false}
                                secureTextEntry={false}
                                onChangeText={value => this.onChangeText('authCode', value)}
                            />
                        </Item>
                        <TouchableOpacity
                            onPress={() => this.confirm()}
                            style={styles.buttonStyle}>
                            <Text style={styles.buttonText}>
                                Confirm
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity
                            onPress={() => this.resendSignUp()}
                            style={[styles.buttonStyle, {
                                backgroundColor: Colors.rust,
                                padding: 4,
                                borderWidth: 0,
                                marginBottom: 4,
                            }]}>
                            <Text style={styles.buttonText}>
                                Resend code
                            </Text>
                        </TouchableOpacity>
                    </View>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}


const styles = StyleSheet.create<{
    container: ViewStyle;
    infoContainer: ViewStyle;
    itemContainer: ViewStyle;
    input: TextStyle;
    itemStyle: ViewStyle;
    iconStyle: ViewStyle;
    buttonStyle: ViewStyle;
    buttonText: TextStyle;
    textStyle: TextStyle;
    countryStyle: ViewStyle;
    closeButtonStyle: ViewStyle;
}>({
    container: {
        ...CommonStyles.container,
        backgroundColor: Colors.stem,
    },
    infoContainer: {
        ...CommonStyles.infoContainer,
        flexDirection: 'column',
    },
    itemContainer: {
        borderRadius: 12,
        padding: 10,
    },
    input: {
        ...CommonStyles.input,
    },
    itemStyle: {
        ...CommonStyles.itemStyle,
        width: 300,
    },
    iconStyle: {
        ...CommonStyles.iconStyle,
    },
    buttonStyle: {
        ...CommonStyles.blackButton,
    },
    buttonText: {
        ...CommonStyles.buttonText,
    },
    textStyle: {
        padding: 5,
        fontSize: 20,
        color: Colors.white,
        fontWeight: 'bold'
    },
    countryStyle: {
        flex: 1,
        backgroundColor: '#5059ae',
        borderTopColor: '#211f',
        borderTopWidth: 1,
        padding: 12,
    },
    closeButtonStyle: {
        flex: 1,
        padding: 12,
        alignItems: 'center',
        backgroundColor: '#b44666',
    }
});
