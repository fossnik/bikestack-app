import React from 'react';
import {
    StyleSheet, TextStyle, ViewStyle,
    Text, View,
    KeyboardAvoidingView, SafeAreaView,
    TouchableOpacity, TouchableWithoutFeedback,
    StatusBar, Keyboard, Alert,
} from 'react-native';
import {Container, Item, Input, Icon} from 'native-base';
import CommonStyles, {Colors} from '../CommonStyles';
import Auth from '@aws-amplify/auth';
import * as helper from '../../Helpers';

export default class SettingsScreen extends React.Component<{
    navigation: any;
}, {}> {
    state = {
        password1: '',
        password2: '',
    };

    onChangeText(key, value) {
        this.setState({[key]: value});
    }

    // Sign out from the app
    signOutAlert = async () => {
        await Alert.alert(
            'Sign Out',
            'Are you sure you want to sign out?',
            [
                {text: 'Cancel', style: 'cancel'},
                {text: 'OK', onPress: () => this.signOut()},
            ],
            {cancelable: false}
        );
    };

    // Confirm sign out
    signOut = async () => {
        await Auth.signOut()
            .then(() => {
                helper.handleSuccess('Sign out complete');
                this.props.navigation.navigate('AuthLoading');
            })
            .catch(err => helper.handleError('An error occurred when signing out', err));
    };

    // Change user password for the app
    changePassword = async () => {
        const {password1, password2} = this.state;
        await Auth.currentAuthenticatedUser()
            .then(user => Auth.changePassword(user, password1, password2))
            .then(() => helper.handleSuccess('Password changed successfully'))
            .catch(err => helper.handleError('An error occurred when setting password', err));
    };

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <StatusBar/>
                <KeyboardAvoidingView enabled style={styles.container} behavior='padding'>
                    <TouchableWithoutFeedback style={styles.container} onPress={Keyboard.dismiss}>
                        <View style={styles.container}>
                            <Container style={styles.infoContainer}>
                                <View style={styles.container}>
                                    {/* Old password */}
                                    <Item rounded style={styles.itemStyle}>
                                        <Icon active name='lock' style={styles.iconStyle}/>
                                        <Input
                                            style={styles.input}
                                            placeholder='Old password'
                                            placeholderTextColor={Colors.placeholder}
                                            returnKeyType='next'
                                            autoCapitalize='none'
                                            autoCorrect={false}
                                            secureTextEntry={true}
                                            // @ts-ignore
                                            onSubmitEditing={(event) => this.refs.SecondInput._root.focus()}
                                            onChangeText={value => this.onChangeText('password1', value)}
                                        />
                                    </Item>
                                    {/* New password */}
                                    <Item rounded style={styles.itemStyle}>
                                        <Icon active name='lock' style={styles.iconStyle}/>
                                        <Input
                                            style={styles.input}
                                            placeholder='New password'
                                            placeholderTextColor={Colors.placeholder}
                                            returnKeyType='go'
                                            autoCapitalize='none'
                                            autoCorrect={false}
                                            secureTextEntry={true}
                                            ref='SecondInput'
                                            onChangeText={value => this.onChangeText('password2', value)}
                                        />
                                    </Item>
                                    <TouchableOpacity
                                        onPress={this.changePassword}
                                        style={styles.buttonStyle}>
                                        <Text style={styles.buttonText}>
                                            Change password
                                        </Text>
                                    </TouchableOpacity>
                                    <View style={{
                                        justifyContent: 'center',
                                        alignItems: 'center',
                                        paddingBottom: 100,
                                    }}/>
                                    <TouchableOpacity
                                        style={[styles.buttonStyle, {
                                            flexDirection: 'row',
                                            justifyContent: 'center',
                                        }]}
                                        onPress={this.signOutAlert}>
                                        <Icon name='md-power' style={{color: Colors.white, paddingRight: 10}}/>
                                        <Text style={styles.buttonText}>
                                            Sign out
                                        </Text>
                                    </TouchableOpacity>
                                </View>
                            </Container>
                        </View>
                    </TouchableWithoutFeedback>
                </KeyboardAvoidingView>
            </SafeAreaView>
        )
    }
}

const styles = StyleSheet.create<{
    container: ViewStyle;
    infoContainer: ViewStyle;
    input: TextStyle;
    itemStyle: ViewStyle;
    iconStyle: ViewStyle;
    buttonStyle: ViewStyle;
    buttonText: TextStyle;
}>({
    container: {
        ...CommonStyles.container,
    },
    infoContainer: {
        ...CommonStyles.infoContainer,
    },
    input: {
        ...CommonStyles.input,
    },
    itemStyle: {
        ...CommonStyles.itemStyle,
    },
    iconStyle: {
        ...CommonStyles.iconStyle,
    },
    buttonStyle: {
        ...CommonStyles.blackButton,
    },
    buttonText: {
        ...CommonStyles.buttonText,
    },
});
