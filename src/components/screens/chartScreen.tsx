import React, {useEffect, useReducer} from 'react';
import moment from 'moment';
import {StyleSheet, Text, TextStyle, View, ViewStyle} from 'react-native';
import _ from 'lodash';
import CommonStyles, {Colors} from '../CommonStyles';
import * as helper from '../../Helpers';
import {ChartMode, Excursion} from '../types';
import ZoomChart from '../zoomChart';
import ActivityIndicator from '../common/activityIndicator';

import API, {graphqlOperation} from '@aws-amplify/api';
import {listExcursions} from '../../graphql/queries';
import {onCreateExcursion} from '../../graphql/subscriptions';
import config from '../../aws-exports';
import Auth from '@aws-amplify/auth';

API.configure(config);

const initialState: {
    excursions: Excursion[];
    yAxis: string;
    mode: ChartMode;
} = {
    excursions: null,
    yAxis: 'distance',
    mode: ChartMode.allData,
};

const reducer = (state, action) => {
    switch (action.type) {
        case 'QUERY':
            const excursions = _.orderBy(action.excursions, (e: Excursion) => new Date(e.date));
            return {...state, excursions};
        case 'SELECTYAXIS':
            return {...state, yAxis: action.yAxis};
        case 'SELECTMODE':
            return {...state, mode: action.mode};
        case 'SUBSCRIPTION':
            return {...state, excursions: [...state.excursions, action.excursion]};
        default:
            return state;
    }
};

function reduceData(excursions, mode) {
    switch (mode) {
        case ChartMode.allData:
            return _.map(excursions, (e: Excursion) => Object.assign({
                date: e.date,
                dateShort: moment.utc(e.date).format('MM/DD'),
                distance: e.distance,
                duration: e.duration,
                speed: Number(((Number(e.distance) / Number(e.duration)) * 60).toFixed(1)),
            }));

        case ChartMode.monthOverMonth:
            const allDates = _.map(excursions, ({date}) => moment(date).format('YYYY-MM'));
            const uniqueMonths = _.uniq(allDates);

            return _.map(uniqueMonths, YYYYMM => {
                const thisMonthData = _.filter(excursions, ({date}) => moment(date).format('YYYY-MM') === YYYYMM);

                const distanceTotal = _.sumBy(thisMonthData, 'distance');
                const durationTotal = _.sumBy(thisMonthData, 'duration');

                return Object.assign({
                    date: moment.utc(YYYYMM).toDate(),
                    dateShort: moment.utc(YYYYMM).format('MMM'),
                    distance: distanceTotal,
                    duration: durationTotal,
                    speed: Number(((distanceTotal / durationTotal) * 60).toFixed(1)),
                });
            });

        default:
            console.error('invalid Chartmode', mode);
    }
}

export default function ChartScreen() {
    const [state, dispatch] = useReducer(reducer, initialState);

    function getData() {
        Auth.currentUserInfo().then(({username}) => {
            API.graphql(graphqlOperation(listExcursions, {filter: {userSub: {eq: username}}}))
                .then(excursionData => {
                    dispatch({type: 'QUERY', excursions: excursionData.data.listExcursions.items});
                    helper.handleSuccess('Excursion query successful - Chartscreen');
                })
                .catch(err => helper.handleError('Error when querying excursion', err));
        })
    }

    useEffect(() => {
        if (_.isNull(state.excursions)) getData();

        const subscription = API.graphql(graphqlOperation(onCreateExcursion)).subscribe({
            next: eventData => {
                const excursion = eventData.value.data.onCreateExcursion;
                dispatch({type: 'SUBSCRIPTION', excursion});
            }
        });

        return () => subscription.unsubscribe();
    });

    const {excursions, mode} = state;

    const isLoading = _.isNull(excursions);
    if (isLoading) return <ActivityIndicator/>;

    const chartData = reduceData(excursions, mode);

    const metricSelector = (
        <View style={{flexDirection: 'row'}}>
            {['distance', 'speed', 'duration'].map(metric => {
                const isActive = metric === state.yAxis;
                return (
                    <Text
                        key={metric}
                        style={isActive ? styles.activeText : styles.mutedText}
                        onPress={() => dispatch({type: 'SELECTYAXIS', yAxis: metric})}
                    >
                        {metric}
                    </Text>
                );
            })}
        </View>
    );

    const monthOverMonthSelector = (
        <View style={{flexDirection: 'row'}}>
            {[ChartMode.allData, ChartMode.monthOverMonth].map(mode => {
                const title = mode === ChartMode.monthOverMonth ? 'Monthly' : 'Daily';
                const isActive = mode === state.mode;
                return (
                    <Text
                        key={mode}
                        style={isActive ? styles.activeText : styles.mutedText}
                        onPress={() => dispatch({
                            type: 'SELECTMODE',
                            mode,
                        })}
                    >
                        {title}
                    </Text>
                )
            })}
        </View>
    );

    const chart = (
        <ZoomChart data={chartData} yAxis={state.yAxis}/>
    );

    return (
        <View style={styles.container}>
            {chart}
            {metricSelector}
            {monthOverMonthSelector}
        </View>
    );
}

const styles = StyleSheet.create<{
    activeText: TextStyle;
    mutedText: TextStyle;
    container: ViewStyle;
}>({
    activeText: {
        color: 'black',
        fontWeight: 'bold',
        padding: '4%',
    },
    mutedText: {
        color: 'grey',
        fontWeight: 'normal',
        padding: '4%',
    },
    container: {
        ...CommonStyles.container,
        backgroundColor: Colors.daisy,
    },
});
