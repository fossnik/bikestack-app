import React, {useReducer} from 'react';
import {
    StyleSheet, TextStyle, ViewStyle,
    TouchableOpacity, Text, View,
} from 'react-native';
import {Icon, Item, Input, Textarea, Container, Form, Content} from 'native-base';
import DateTimePicker from 'react-native-modal-datetime-picker';
import CommonStyles, {Colors, semiotics} from '../CommonStyles';
import * as helper from '../../Helpers';
import {Excursion} from '../types';
import DurationPicker from '../common/durationPicker';
import _ from 'lodash';

import API, {graphqlOperation} from '@aws-amplify/api';
import Auth from '@aws-amplify/auth';
import {createExcursion} from '../../graphql/mutations';
import config from '../../aws-exports';

API.configure(config);

const initialState: {
    entryData: Excursion;
    isDateTimePickerVisible: boolean;
} = {
    entryData: {
        date: new Date(),
        distance: null,
        duration: null,
        notes: null,
    },
    isDateTimePickerVisible: false,
};

const reducer = (state, action) => {
    switch (action.type) {
        case 'ON_CHANGE':
            const validActions = helper.fourMetrics;
            const changeType = _.find(Object.keys(action), key => validActions.includes(key));
            const actionData = action[changeType];
            return _.isUndefined(actionData) ? state : {
                ...state,
                entryData: {
                    ...state.entryData,
                    [changeType]: actionData,
                },
            };

        case 'DATETIME_VISIBILITY':
            return {...state, isDateTimePickerVisible: action.visible};

        case 'ON_SUBMIT_SUCCESS':
            return initialState;

        default:
            return state;
    }
};

export default function NewEntryScreen() {
    const [state, dispatch] = useReducer(reducer, initialState);

    function _handleSubmit(entryData) {
        if (helper.validateEntryData(entryData)) {
            Auth.currentUserInfo().then(({username}) => {
                API.graphql(graphqlOperation(createExcursion, {input: {...entryData, userSub: username}}))
                    .then(() => {
                        helper.handleSuccess('New excursion submitted successfully', true);
                        dispatch({type: 'ON_SUBMIT_SUCCESS'});
                    })
                    .catch(err => helper.handleError('An error occurred when submitting log', err))
            })
        }
    }

    return <Container style={styles.container}>
        <DateTimePicker
            isVisible={state.isDateTimePickerVisible}
            onConfirm={date => {
                dispatch({type: 'ON_CHANGE', date});
                dispatch({type: 'DATETIME_VISIBILITY', visible: false});
            }}
            onCancel={() => dispatch({type: 'DATETIME_VISIBILITY', visible: false})}
        />
        <Content padder style={{width: '100%'}}>
            <Form style={styles.form}>
                <Item
                    rounded
                    style={styles.dateStyle}
                    onPress={() => dispatch({type: 'DATETIME_VISIBILITY', visible: true})}
                >
                    <Icon name={semiotics.date.icon} style={styles.iconStyle}/>
                    <Text>{state.entryData.date.toLocaleDateString()}</Text>
                </Item>
                <View style={{flexDirection: 'row'}}>
                    <Item rounded style={styles.halfItem}>
                        <Icon name='md-map' style={styles.iconStyle}/>
                        <Input
                            placeholder='distance'
                            placeholderTextColor={Colors.placeholder}
                            keyboardType='numeric'
                            returnKeyType='done'
                            autoCapitalize='none'
                            autoCorrect={false}
                            secureTextEntry={false}
                            value={state.entryData.distance}
                            style={styles.input}
                            maxLength={4}
                            onChangeText={distance => dispatch({type: 'ON_CHANGE', distance})}
                        />
                    </Item>
                    <Item rounded style={styles.halfItem}>
                        <Icon name='md-hourglass' style={styles.iconStyle}/>
                        <DurationPicker
                            value={state.entryData.duration}
                            onChange={duration => dispatch({type: 'ON_CHANGE', duration})}
                        />
                    </Item>
                </View>
                <Item rounded style={styles.itemStyle}>
                    <Icon name='md-paper' style={styles.iconStyle}/>
                    <Textarea
                        bordered
                        underline
                        style={{
                            borderColor: styles.itemStyle.backgroundColor,
                            borderRadius: 24,
                            flex: 1,
                            ...styles.input,
                        }}
                        rowSpan={5}
                        placeholder='notes'
                        placeholderTextColor={Colors.placeholder}
                        returnKeyType='done'
                        autoCapitalize='none'
                        autoCorrect={false}
                        value={state.entryData.notes}
                        onChangeText={notes => dispatch({type: 'ON_CHANGE', notes})}
                    />
                </Item>
            </Form>
            <TouchableOpacity
                onPress={() => _handleSubmit(state.entryData)}
                style={styles.submitButton}
            >
                <Text style={styles.buttonText}>Submit</Text>
            </TouchableOpacity>
        </Content>
    </Container>
}

const styles = StyleSheet.create<{
    halfItem: ViewStyle;
    form: ViewStyle;
    input: ViewStyle;
    iconStyle: ViewStyle;
    itemStyle: ViewStyle;
    dateStyle: ViewStyle;
    container: ViewStyle;
    submitButton: ViewStyle;
    buttonText: TextStyle;
}>({
    halfItem: {
        ...CommonStyles.itemStyle,
        flex: 1,
        marginHorizontal: '2%',
    },
    form: {
        ...CommonStyles.form,
    },
    input: {
        ...CommonStyles.input,
    },
    iconStyle: {
        ...CommonStyles.iconStyle,
    },
    itemStyle: {
        ...CommonStyles.itemStyle,
    },
    dateStyle: {
        ...CommonStyles.itemStyle,
        width: '40%',
        padding: '1.5%',
        justifyContent: 'center',
        alignSelf: 'center',
    },
    container: {
        ...CommonStyles.container,
    },
    submitButton: {
        ...CommonStyles.blackButton,
    },
    buttonText: {
        ...CommonStyles.buttonText,
    },
});
