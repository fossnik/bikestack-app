import React, {useEffect, useReducer} from 'react';
import {StyleSheet, TextStyle, View, ViewStyle,} from 'react-native';
import _ from 'lodash';
import CommonStyles, {Colors} from '../CommonStyles';
import ExcursionViewModal from '../common/excursionViewModal';
import * as helper from '../../Helpers';
import {Excursion} from '../types';

import API, {graphqlOperation} from '@aws-amplify/api';
import PubSub from '@aws-amplify/pubsub';
import {listExcursions} from '../../graphql/queries';
import {onCreateExcursion} from '../../graphql/subscriptions';
import config from '../../aws-exports';
import {Content, Text, Container, List, ListItem} from 'native-base';
import ActivityIndicator from '../common/activityIndicator';
import Auth from '@aws-amplify/auth';

API.configure(config);
PubSub.configure(config);

const initialState: {
    excursions: Excursion[];
    selectedExcursion: { item: Excursion, index: number };
} = {
    excursions: null,
    selectedExcursion: null,
};

const reducer = (state, action) => {
    switch (action.type) {
        case 'QUERY':
            return {...state, excursions: action.excursions};
        case 'SELECT_EXCURSION':
            return {...state, selectedExcursion: action.selectedExcursion};
        case 'SUBSCRIPTION':
            return {...state, excursions: [...state.excursions, action.excursion]};
        default:
            return state;
    }
};

export default function TripLogScreen() {
    const [state, dispatch] = useReducer(reducer, initialState);

    function getData() {
        Auth.currentUserInfo().then(({username}) => {
            API.graphql(graphqlOperation(listExcursions, {filter: {userSub: {eq: username}}}))
                .then(excursionData => {
                    dispatch({type: 'QUERY', excursions: excursionData.data.listExcursions.items});
                    helper.handleSuccess('Excursion query successful');
                })
                .catch(err => helper.handleError('Error when querying excursion', err));
        })
    }

    useEffect(() => {
        getData();

        const subscription = API.graphql(graphqlOperation(onCreateExcursion)).subscribe({
            next: eventData => {
                const excursion = eventData.value.data.onCreateExcursion;
                dispatch({type: 'SUBSCRIPTION', excursion});
            }
        });

        return () => subscription.unsubscribe();
    }, []);

    const isLoading = _.isNull(state.excursions);
    if (isLoading) return <ActivityIndicator/>;

    function listOfExcursions(excursions: Excursion[]) {
        const excursionsSortedByDate = _.orderBy(excursions, 'date', 'desc');

        function getEnglishMonthName(date: Date) {
            return [
                'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'
            ][date.getMonth()]
        }

        function getMonthYearString(date: Date) {
            const dateObj = new Date(date);
            return getEnglishMonthName(dateObj) + ' ' + dateObj.getFullYear();
        }

        function shouldPrefixMonthYearHeader(index: number) {
            if (index === 0) return true;
            const [date1, date2] = [
                excursionsSortedByDate[index].date,
                excursionsSortedByDate[index - 1].date,
            ];
            return getMonthYearString(date1) !== getMonthYearString(date2);
        }

        return _.map(excursionsSortedByDate, (item, index) => {
            const {distance, id}: Excursion = item,
                date = new Date(item.date);

            const headerTextStr = getMonthYearString(date);

            const monthYearHeader = (
                <ListItem itemDivider style={styles.listItem}>
                    <Text style={styles.bold}>{headerTextStr}</Text>
                </ListItem>
            );

            const listItemText = (
                <View style={styles.listItemText}>
                    <Text>{date.toLocaleDateString()}</Text>
                    <Text> - </Text>
                    <Text style={styles.bold}>{distance} miles</Text>
                </View>
            );

            return (
                <React.Fragment key={id}>
                    {shouldPrefixMonthYearHeader(index) && monthYearHeader}
                    <ListItem
                        onPress={() => dispatch({
                            type: 'SELECT_EXCURSION',
                            selectedExcursion: {item, index},
                        })}
                        style={styles.listItem}
                    >
                        {listItemText}
                    </ListItem>
                </React.Fragment>
            );
        });
    }

    const excursionList = (
        <Content style={{
            backgroundColor: Colors.daisy,
        }}>
            <List style={{
                marginHorizontal: '15%',
            }}>
                {listOfExcursions(state.excursions)}
            </List>
        </Content>
    );

    const excursionViewModal = (
        <ExcursionViewModal selected={state.selectedExcursion} unsetVisible={() => dispatch({
            type: 'SELECT_EXCURSION',
            selectedExcursion: null,
        })}/>
    );

    return (
        <View style={styles.container}>
            <Container>
                {!_.isEmpty(state.excursions) && excursionList}
                {!_.isNull(state.selectedExcursion) && excursionViewModal}
            </Container>
        </View>
    );
}

const styles = StyleSheet.create<{
    icon: ViewStyle;
    bold: TextStyle;
    listItem: ViewStyle;
    listItemText: ViewStyle;
    container: ViewStyle;
    modalView: ViewStyle;
}>({
    icon: {
        textAlign: 'center',
    },
    bold: {
        fontWeight: 'bold',
    },
    listItem: {
        justifyContent: 'space-evenly',
        borderRadius: 16,
    },
    listItemText: {
        flexDirection: 'row',
        padding: 4,
        paddingHorizontal: '15%',
    },
    container: {
        ...CommonStyles.container,
    },
    modalView: {
        flex: 1,
        padding: '4%',
        margin: '4%',
        backgroundColor: Colors.white,
        borderWidth: 2,
        borderColor: Colors.gunmetal,
        textAlign: 'center',
    },
});
