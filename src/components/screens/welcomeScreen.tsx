import React from 'react';
import {
    StyleSheet, TextStyle, ViewStyle, ImageStyle,
    TouchableOpacity, Text, View, Image,
} from 'react-native';
import CommonStyles, {Colors} from '../CommonStyles';
const logo = require('../images/logo.png');

export default class WelcomeScreen extends React.Component<{
    navigation: any;
}, {}> {
    handleRoute = async (destination) => {
        await this.props.navigation.navigate(destination)
    };

    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.appTitle}>Bike Stack</Text>
                <Image
                    style={styles.logo}
                    source={logo}
                />
                <View style={styles.buttonsContainer}>
                    <TouchableOpacity
                        onPress={() => this.handleRoute('SignUp')}
                        style={styles.button}>
                        <Text style={styles.pageLink}>Sign up</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.handleRoute('SignIn')}
                        style={styles.button}>
                        <Text style={styles.pageLink}>Sign in</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        onPress={() => this.handleRoute('ForgetPassword')}
                    >
                        <Text style={styles.pageLink}>Forget password ?</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create<{
    container: ViewStyle;
    logo: ImageStyle;
    appTitle: TextStyle;
    pageLink: TextStyle;
    buttonsContainer: ViewStyle;
    button: ViewStyle;
}>({
    container: {
        backgroundColor: Colors.stem,
        height: '100%',
    },
    logo: {
        height: '30%',
    },
    appTitle: {
        textAlign: 'center',
        fontSize: 40,
        padding: 16,
        margin: 22,
        paddingHorizontal: 20,
        borderRadius: 50,
        backgroundColor: Colors.apricot,
    },
    pageLink: {
        textAlign: 'center',
        paddingHorizontal: 20,
        fontSize: 20,
        color: Colors.white,
    },
    buttonsContainer: {
        width: '100%',
        height: '50%',
        backgroundColor: Colors.rust,
        justifyContent: 'space-evenly',
        position: 'absolute',
        bottom: 0,
    },
    button: {
        ...CommonStyles.blackButton,
    },
});
