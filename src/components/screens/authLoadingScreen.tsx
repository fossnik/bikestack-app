import React from 'react';
import {View, ActivityIndicator} from 'react-native';
import CommonStyles, {Colors} from '../CommonStyles';
import Auth, {CognitoUser} from '@aws-amplify/auth';
import * as helper from '../../Helpers';

export default class AuthLoadingScreen extends React.Component<{
    navigation: any;
}, {}> {
    state = {
        userToken: null,
    };

    loadApp = async () => {
        await Auth.currentAuthenticatedUser()
            .then((user: CognitoUser | any) => {
                this.setState({userToken: user.signInUserSession.accessToken.jwtToken});
            })
            .catch(err => err != 'not authenticated' && helper.handleError('An error occurred during authentication', err));

        this.props.navigation.navigate(this.state.userToken ? 'App' : 'Auth');
    };

    componentDidMount = async () => await this.loadApp();

    render = () =>
        <View style={CommonStyles.container}>
            <ActivityIndicator size='large' color={Colors.white}/>
        </View>
}
