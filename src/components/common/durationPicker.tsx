import React, {useState} from 'react';
import {StyleSheet, Text, TextStyle} from 'react-native';
import DateTimePicker from 'react-native-modal-datetime-picker';
import moment from "moment";
import CommonStyles, {Colors} from '../CommonStyles';

const timeOffset = new Date().getTimezoneOffset() * 60 * 1000;

export default function (props: {
    value: number | null;
    onChange: (number) => void;
}) {
    const [isVisible, setVisibility] = useState(false);

    const setTime = time => {
        if (time !== undefined) {
            const minutes = moment(time).diff(moment(new Date(timeOffset))) / 60 / 1000;
            props.onChange(minutes);
            setVisibility(false);
        }
    };

    const duration = props.value;

    return (
        <React.Fragment>
            <Text
                style={[styles.textStyle, !duration && styles.muted]}
                onPress={() => setVisibility(true)}
            >
                {duration ? `${moment.duration(duration * 60 * 1000).asMinutes()} minutes` : 'duration'}
            </Text>
            <DateTimePicker
                isVisible={isVisible}
                onConfirm={setTime}
                date={new Date(timeOffset)}
                mode={'time'}
                timePickerModeAndroid='spinner'
                onCancel={() => setVisibility(false)}
            />
        </React.Fragment>
    );
}

const styles = StyleSheet.create<{
    textStyle: TextStyle;
    muted: TextStyle;
}>({
    textStyle: {
        width: '50%',
        padding: 10,
        ...CommonStyles.input,
    },
    muted: {
        color: Colors.placeholder,
    },
});
