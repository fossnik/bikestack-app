import React from 'react';
import {Modal, StyleSheet, TextStyle, TouchableWithoutFeedback, ViewStyle,} from 'react-native';
import _ from 'lodash';
import {Colors, semiotics} from '../CommonStyles';
import * as helper from '../../Helpers';
import {Excursion} from '../types';

import {Body, Card, CardItem, Container, Content, Icon, Text} from 'native-base';

export default function ExcursionViewModal(props: {
   selected: {
       index: number;
       item: Excursion;
   },
   unsetVisible: () => void;
}) {
    const {index, item} = props.selected;

    const date = (new Date(item.date)).toDateString();
    item.speed = Number((((item.distance / item.duration) * 60)).toFixed(2));

    return (
        <Modal
            animationType='slide'
            transparent={true}
        >
            <TouchableWithoutFeedback
                onPress={props.unsetVisible}
            >
                <Container style={styles.modalView}>
                    <Content padder>
                        <Card>
                            <CardItem header bordered>
                                <Body>
                                    <Text>
                                        Outing #{Number(index) + 1}
                                    </Text>
                                    <Text note>{date}</Text>
                                </Body>
                            </CardItem>
                            {_.map(['duration', 'distance', 'speed'], metric => (
                                <CardItem key={metric}>
                                    <Icon active name={semiotics[metric].icon} style={styles.icon}/>
                                    <Text style={[styles.bold, {color: semiotics[metric].color}]}>
                                        {helper.titleCase(metric)}: {item[metric]} {semiotics[metric].unit}
                                    </Text>
                                </CardItem>
                            ))}
                            <CardItem bordered footer>
                                <Body>
                                    <Icon active name='md-paper' style={styles.icon}/>
                                    <Text>
                                        {item.notes}
                                    </Text>
                                </Body>
                            </CardItem>
                        </Card>
                    </Content>
                </Container>
            </TouchableWithoutFeedback>
        </Modal>
    );
}

const styles = StyleSheet.create<{
    icon: ViewStyle;
    bold: TextStyle;
    modalView: ViewStyle;
}>({
    icon: {
        textAlign: 'center',
    },
    bold: {
        fontWeight: 'bold',
    },
    modalView: {
        flex: 1,
        padding: '4%',
        margin: '4%',
        backgroundColor: Colors.white,
        borderWidth: 2,
        borderColor: Colors.gunmetal,
        textAlign: 'center',
    },
});
