import React from 'react';
import {ActivityIndicator, View, StyleSheet, ViewStyle} from 'react-native';
import {Colors} from '../CommonStyles';

export default function() {
    return (
        <View style={styles.viewStyle}>
            <ActivityIndicator
                size='large'
                color={Colors.blue}
            />
        </View>
    );
}

const styles = StyleSheet.create<{
    viewStyle: ViewStyle;
}>({
    viewStyle: {
        flex: 1,
        alignContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        justifyContent: 'center',
    },
});
