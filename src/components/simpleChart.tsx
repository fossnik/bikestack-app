import React from 'react';
import {Excursion} from './types';
import {VictoryBar, VictoryChart, VictoryTheme, VictoryGroup} from 'victory-native';
import _ from 'lodash';
import {semiotics} from './CommonStyles';

export default function SimpleChart(props: {
    data: Excursion[];
    yAxis: string;
}) {
    const {yAxis, data} = props;

    const chartData = _.map(data, (e: Excursion) => Object.assign({
        date: e.dateShort,
        distance: e.distance,
        duration: e.duration,
        speed: Number(((e.distance / e.duration) * 60).toFixed(1)),
    }));

    return (
        <VictoryChart
            width={350}
            theme={VictoryTheme.material}
        >
            <VictoryGroup
                color={semiotics[yAxis].color}
            >
                <VictoryBar
                    data={chartData}
                    x='date'
                    y={yAxis}
                />
            </VictoryGroup>
        </VictoryChart>

    );
}
