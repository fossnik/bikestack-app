import React, {useState} from 'react';
import {StyleSheet, Text, TextStyle, View, ViewStyle} from 'react-native';
import {Excursion} from './types';
import {VictoryBar, VictoryChart, VictoryLabel} from 'victory-native';
import _ from 'lodash';
import MultiSlider from '@ptomasroos/react-native-multi-slider';
import {semiotics} from './CommonStyles';
import moment from 'moment';

export default function ZoomChart(props: {
    data: Excursion[];
    yAxis: string;
}) {
    const {yAxis, data} = props;
    const isSliderActive = data.length > 5;
    const maxZoom = isSliderActive ? data.length - 5 : 0;
    const [visibleRange, setRange] = useState([maxZoom, data.length - 1]);
    const [rangeDisplay, setRangeDisplay] = useState(visibleRange);

    function onRangeChange(r) {
        setRange(r);
    }

    function onRangeDisplayChange(r) {
        setRangeDisplay(r);
    }

    function getData() {
        return isSliderActive ? _.slice(data, visibleRange[0], visibleRange[1] + 1) : data;
    }

    function getZoomSlider() {
        const [startDate, endDate] = rangeDisplay.map(index => data[index]
            ? moment.utc(data[index].date).format('YYYY-MM-DD')
            : null
        );

        const dateRange = (
            <View style={{flexDirection: 'row'}}>
                <Text style={rangeDisplay[0] !== visibleRange[0] && styles.activeText}>
                    {startDate}
                </Text>
                <Text>{'   <->   '}</Text>
                <Text style={rangeDisplay[1] !== visibleRange[1] && styles.activeText}>
                    {endDate}
                </Text>
            </View>
        );

        return (
            <React.Fragment>
                {dateRange}
                <MultiSlider
                    values={[rangeDisplay[0], rangeDisplay[1]]}
                    onValuesChange={onRangeDisplayChange}
                    onValuesChangeFinish={onRangeChange}
                    min={0}
                    max={data.length - 1}
                    snapped={true}
                    isMarkersSeparated={true}
                />
            </React.Fragment>
        );
    }

    const labelComponent = (
        <VictoryLabel textAnchor='start' dx={data => 8 - String(data.datum[yAxis]).length * 2}/>
    );

    const chartData = getData();

    const victoryBar = (
        <VictoryBar
            alignment='start'
            style={{
                data: {
                    fill: semiotics[yAxis].color,
                },
            }}
            labels={({datum}) => datum[yAxis]}
            data={chartData}
            x='dateShort'
            y={yAxis}
            labelComponent={labelComponent}
        />
    );

    return (
        <React.Fragment>
            <VictoryChart>
                {victoryBar}
            </VictoryChart>
            {isSliderActive && getZoomSlider()}
        </React.Fragment>
    );
}

const styles = StyleSheet.create<{
    activeText: TextStyle;
}>({
    activeText: {
        fontWeight: 'bold',
    },
});
