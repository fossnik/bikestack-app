import {ImageStyle, StyleSheet, TextStyle, ViewStyle} from 'react-native';
import Colors from './Colors';

export {Colors};

export default StyleSheet.create<{
  form: ViewStyle;
  blackButton: ViewStyle;
  container: ViewStyle;
  infoContainer: ViewStyle;
  logo: ImageStyle;
  buttonText: TextStyle;
  input: TextStyle;
  itemStyle: ViewStyle;
  iconStyle: ViewStyle;
}>({
  form: {
    backgroundColor: Colors.stem,
    color: Colors.stem,
  },
  blackButton: {
    alignItems: 'center',
    backgroundColor: Colors.navy,
    padding: 14,
    marginBottom: 20,
    borderRadius: 24,
    marginHorizontal: 60,
    borderWidth: 2,
    borderColor: Colors.apricot,
  },
  container: {
    width: '100%',
    height: '100%',
    backgroundColor: Colors.stem,
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  infoContainer: {
    padding: 20,
    margin: 20,
    borderRadius: 8,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 30,
    backgroundColor: Colors.stem,
  },
  logo: {
    width: 250,
    height: 250,
    margin: 20,
    borderRadius: 40,
    alignSelf: 'center',
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: Colors.white,
    textAlign: 'center',
  },
  input: {
    fontSize: 16,
    fontWeight: 'bold',
    color: Colors.gunmetal,
  },
  itemStyle: {
    marginBottom: 10,
    backgroundColor: Colors.daisy,
  },
  iconStyle: {
    color: Colors.gunmetal,
    fontSize: 28,
    marginLeft: 15
  },
});

export const semiotics = {
  distance: {
    color: Colors.green,
    icon: 'md-map',
    unit: 'miles',
  },
  duration: {
    color: Colors.blue,
    icon: 'md-hourglass',
    unit: 'minutes',
  },
  speed: {
    color: Colors.red,
    icon: 'md-speedometer',
    unit: 'mph'
  },
  date: {
    icon: 'md-hourglass',
  },
  notes: {
    icon: 'md-paper',
  }
};
