// tslint:disable
// this is an auto generated file. This will be overwritten

export const createExcursion = `mutation CreateExcursion($input: CreateExcursionInput!) {
  createExcursion(input: $input) {
    id
    userSub
    distance
    duration
    date
    notes
  }
}
`;
export const updateExcursion = `mutation UpdateExcursion($input: UpdateExcursionInput!) {
  updateExcursion(input: $input) {
    id
    userSub
    distance
    duration
    date
    notes
  }
}
`;
export const deleteExcursion = `mutation DeleteExcursion($input: DeleteExcursionInput!) {
  deleteExcursion(input: $input) {
    id
    userSub
    distance
    duration
    date
    notes
  }
}
`;
