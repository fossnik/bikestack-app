// tslint:disable
// this is an auto generated file. This will be overwritten

export const getExcursion = `query GetExcursion($id: ID!) {
  getExcursion(id: $id) {
    id
    userSub
    distance
    duration
    date
    notes
  }
}
`;
export const listExcursions = `query ListExcursions(
  $filter: ModelExcursionFilterInput
  $limit: Int
  $nextToken: String
) {
  listExcursions(filter: $filter, limit: $limit, nextToken: $nextToken) {
    items {
      id
      userSub
      distance
      duration
      date
      notes
    }
    nextToken
  }
}
`;
