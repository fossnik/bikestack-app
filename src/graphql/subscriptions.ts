// tslint:disable
// this is an auto generated file. This will be overwritten

export const onCreateExcursion = `subscription OnCreateExcursion {
  onCreateExcursion {
    id
    userSub
    distance
    duration
    date
    notes
  }
}
`;
export const onUpdateExcursion = `subscription OnUpdateExcursion {
  onUpdateExcursion {
    id
    userSub
    distance
    duration
    date
    notes
  }
}
`;
export const onDeleteExcursion = `subscription OnDeleteExcursion {
  onDeleteExcursion {
    id
    userSub
    distance
    duration
    date
    notes
  }
}
`;
